//
//  GroupCollectionViewCell.swift
//  MemberGroups
//
//  Created by Yvonne.H on 2018/7/7.
//  Copyright © 2018年 Yvonne.H. All rights reserved.
//

import UIKit
import RxSwift
import Kingfisher

class GroupCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    private(set) var disposeBag = DisposeBag()
    static let CORNER_RADIUS: CGFloat = 3
    static let SHADOW_OPACITY: Float = 0.2
    var data: Group? {
        didSet { setCellViews() }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.cornerRadius = GroupCollectionViewCell.CORNER_RADIUS
        addShadow(opacity: GroupCollectionViewCell.SHADOW_OPACITY)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        photoImageView.setCircleStyle()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
    
    func setCellViews() {
        guard let dataUnwrap = data else { return }
        photoImageView.backgroundColor = UIColor.HaHaGoBlue
        nameLabel.text = dataUnwrap.name
        photoImageView.kf.setImage(with: URL(string: dataUnwrap.photo)) {
            [weak self] (image, error, cacheType, imageURL) in
            self?.photoImageView.image = image
        }
    }
}
