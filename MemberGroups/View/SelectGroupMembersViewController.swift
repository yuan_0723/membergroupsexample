//
//  SelectGroupMembersViewController.swift
//  MemberGroups
//
//  Created by Yvonne.H on 2018/7/9.
//  Copyright © 2018年 Yvonne.H. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class SelectGroupMembersViewController: UIViewController {

    @IBOutlet weak var searchResultCountLabel: UILabel!
    @IBOutlet weak var selectedUserCountLabel: UILabel!
    @IBOutlet weak var searchResultTableView: UITableView!
    @IBOutlet weak var selectedUserCollectionView: UICollectionView!
    
    let disposeBag = DisposeBag()
    let viewModel = AppDelegate.getUIAppDelegate().viewModelContainer.resolve(SelectGroupMembersViewModel.self)
    var nextBarButton: UIBarButtonItem?
    var searchResultTableViewDataSource: RxTableViewSectionedReloadDataSource<SectionOfUser>!
    var selectedUserTableViewDataSource: RxCollectionViewSectionedAnimatedDataSource<SectionOfUser>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarItem()
        setupSearchController()
        setupSearchResultTableView()
        setupSelectedUsersCollectionView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func pressedNextBarButton() {
        guard let viewController = getVCByStoryboardID("EditGroupDetailViewController") as? EditGroupDetailViewController else {
            fatalError(getDebugInfo(MemberGroupsError.unwrapError.localizedDescription, #function, #file, #line))
        }
        viewController.viewModel?.setupJoinedUsersData(otherUsers: viewModel?.selectedUsersData ?? [])
        pushToTargetVC(viewController)
    }
    
    func setupSearchController() {
        initSearchController()
        observeSerchBarEvents()
    }
    
    func setupSearchResultTableView() {
        setupCellInSearchResultTableView()
        observeSearchResultTableViewEvents()
        dataBindingWithSearchResultTableView()
    }
    
    func setupNavigationBarItem() {
        title = "勾選成員"
        nextBarButton = UIBarButtonItem(title: "下一步", style: UIBarButtonItemStyle.plain, target: self, action: #selector(pressedNextBarButton))
        nextBarButton?.isEnabled = false
        navigationItem.setRightBarButton(nextBarButton, animated: true)
    }
    
    func initSearchController() {
        let searchController = UISearchController(searchResultsController: nil)
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.dimsBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
    }
    
    func dataBindingWithSearchResultTableView() {
        viewModel?.searchResultReplaySubject
            .asObserver()
            .bind(to: searchResultTableView.rx.items(dataSource: searchResultTableViewDataSource))
            .disposed(by: disposeBag)
    }
    
    func dataBindingWithSelectedUserCollectionView() {
        viewModel?.selectedUsersBehaviorSubject
            .asObservable()
            .bind(to: selectedUserCollectionView.rx.items(dataSource: selectedUserTableViewDataSource))
            .disposed(by: disposeBag)
    }
    
    func setupSelectedUsersCollectionView() {
        setupCellInSelectedUserCollectionView()
        dataBindingWithSelectedUserCollectionView()
    }
    
    func setupCellInSearchResultTableView() {
        searchResultTableView.registerXIBCell("UserTableViewCell")
        searchResultTableViewDataSource =
            RxTableViewSectionedReloadDataSource<SectionOfUser>(
                configureCell: { (dataSource, tableView, indexPath, user) in
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCell", for: indexPath) as? UserTableViewCell
                    else { return UITableViewCell() }
                    cell.data = user
                    return cell
            })
    }
    
    func setupCellInSelectedUserCollectionView() {
        selectedUserCollectionView.registerXIBCell("UserCollectionViewCell")
        selectedUserTableViewDataSource =
            RxCollectionViewSectionedAnimatedDataSource<SectionOfUser>(
                configureCell: { (dataSource, collectionView, indexPath, user) in
                    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserCollectionViewCell", for: indexPath) as? UserCollectionViewCell
                    else { return UICollectionViewCell() }
                    cell.data = user
                    return cell },
                
                configureSupplementaryView:{ (dataSource, collectionView, header, indexPath) in
                    let section = collectionView.dequeueReusableSupplementaryView(ofKind: header, withReuseIdentifier: "UserCollectionViewSection", for: indexPath)
                    return section
        })
    }
    
    func observeSerchBarEvents() {
        guard let navigarionSearchBar = navigationItem.searchController?.searchBar else { return }
        
        navigarionSearchBar
            .rx
            .text
            .orEmpty
            .debounce(0.5, scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .asDriver(onErrorJustReturn: "")
            .drive(
                onNext: { [weak self] (searchText) in
                    guard let strongSelf = self else { return }
                    strongSelf.viewModel?.searchUsersData(searchText: searchText)},
                onCompleted: { debugPrint("SearchBar Text Observable Complete") },
                onDisposed: { debugPrint("SearchBar Text Observable Disposed")})
            .disposed(by: disposeBag)
        
        navigarionSearchBar
            .rx
            .cancelButtonClicked
            .asDriver()
            .drive(
                onNext: { [weak self] (_) in
                    guard let strongSelf = self else { return }
                    strongSelf.viewModel?.searchUsersData(searchText: "")},
                onCompleted: { debugPrint("SearchBar Cancel Observable Complete") },
                onDisposed: { debugPrint("SearchBar Cancel Observable Disposed")})
            .disposed(by: disposeBag)
    }
    
    func observeSearchResultTableViewEvents() {
        searchResultTableView.rx.itemSelected.asDriver()
            .drive(
                onNext: { [weak self] (indexPath) in
                    guard let strongSelf = self,
                        let itemData = self?.searchResultTableViewDataSource.sectionModels[indexPath.section].items[indexPath.row]
                        else { return }
                    let willUserJoin = !itemData.hasJoin
                    strongSelf.viewModel?.updateUserJoinStatus(user: itemData, willUserJoin: willUserJoin)},
                onCompleted: { debugPrint("Search Result Table View Item Select Observable Complete") },
                onDisposed: { debugPrint("Search Result Table View Item Select Observable Disposed") })
            .disposed(by: disposeBag)
        
        searchResultTableView
            .rx
            .didScroll
            .asDriver()
            .drive(
                onNext: { [weak self] (_) in
                    guard let strongSelf = self else { return }
                    strongSelf.navigationItem.searchController?.searchBar.resignFirstResponder()},
                onCompleted: { debugPrint("Search TableView Dismiss Keyboard Observable Complete") },
                onDisposed: { debugPrint("Search TableView Dismiss Keyboard Observable Disposed")})
            .disposed(by: disposeBag)
        
        viewModel?.searchResultReplaySubject
            .asDriver(onErrorJustReturn: [SectionOfUser(header: "", items: [])])
            .map { (data) -> Int in
                return data[0].items.count}
            .distinctUntilChanged()
            .delay(0.0001)
            .drive(
                onNext: { [weak self] (searchResultCounts) in
                    guard let strongSelf = self else { return }
                    strongSelf.searchResultCountLabel.text = String(searchResultCounts)
                    if searchResultCounts == 0 { return }
                    strongSelf.searchResultTableView.scrollToTopRow(animated: false) },
                onCompleted: { debugPrint("Search Result Data Count Observable Complete") },
                onDisposed: { debugPrint("Search Result Data Count Observable Disposed") })
            .disposed(by: disposeBag)
        
        viewModel?.selectedUsersBehaviorSubject
            .map { (data) -> Void in }
            .debounce(0.3, scheduler: MainScheduler.instance)
            .delay(0.000001, scheduler: MainScheduler.instance)
            .asDriver(onErrorJustReturn: ())
            .drive(
                onNext: { [weak self] (_) in
                    guard let strongSelf = self else { return }
                    if strongSelf.selectedUserCollectionView.numberOfItems(inSection: 0) > 1 {
                        let lastItemIndexPath = IndexPath(row: (strongSelf.selectedUserCollectionView.numberOfItems(inSection: 0)-1), section: 0)
                        strongSelf.selectedUserCollectionView.scrollToItem(at: lastItemIndexPath, at: .right, animated: true)
                    }},
                onCompleted: { debugPrint("Search Result CollectionView Scroll Observable Complete") },
                onDisposed: { debugPrint("Search Result CollectionView Observable Disposed")})
            .disposed(by: disposeBag)
        
        viewModel?.selectedUsersBehaviorSubject
            .asDriver(onErrorJustReturn: [SectionOfUser(header: "", items: [])])
            .drive(
                onNext: { [weak self](data) in
                    guard let strongSelf = self,
                        !data[0].items.isEmpty
                        else { return }
                    let isSelectedUserEmpty = data[0].items[0].isPlaceHolder()
                    strongSelf.nextBarButton?.isEnabled = !isSelectedUserEmpty
                    strongSelf.selectedUserCountLabel.text = isSelectedUserEmpty ? "0" : String(data[0].items.count)},
                onCompleted: { debugPrint("Search Result CollectionView Scroll Observable Complete") },
                onDisposed: { debugPrint("Search Result CollectionView Observable Disposed")})
            .disposed(by: disposeBag)
    }
}
