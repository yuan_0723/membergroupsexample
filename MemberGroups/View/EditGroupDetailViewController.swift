//
//  EditGroupDetailViewController.swift
//  MemberGroups
//
//  Created by Yvonne.H on 2018/7/11.
//  Copyright © 2018年 Yvonne.H. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit
import Photos

class EditGroupDetailViewController: UIViewController {
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    static let EDIT_VIEW_HEIGHT = 130
    var groupPhotoImageView: UICircleImageView!
    var groupNameTextfield: UITextField!
    var loadingView: LoadingView?
    var selecPhotoAlertController: UIAlertController?
    let viewModel = AppDelegate.getUIAppDelegate().viewModelContainer.resolve(EditGroupDetailViewModel.self)
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        layoutEditableViews()
        layoutJoinedMembers()
        setupNavigationBarItem()
        setupScrollView()
        setupGroupNameTextField()
        dataBindingWithGroupPhoto()
        setupSelectPhotoAlertController()
    }
    
    @objc func tapBackgroundView (_ sender: UITapGestureRecognizer) {
        dismissKeyboard()
    }
    
    @objc func pressedDoneBarButton() {
        dismissKeyboard()
        showLoadingView()
        viewModel?.getCreateGroupDataObservable()
            .subscribe(
                onSuccess: { [weak self] (group) in
                    self?.loadingView?.stop()
                    self?.navigationController?.popToRootViewController(animated: true)},
                onError: { [weak self] (error) in
                    self?.loadingView?.stop()
                    debugPrint(error.localizedDescription)})
            .disposed(by: disposeBag)
    }
    
    @objc func pressedGroupPhotoImageView(_ sender: UICircleImageView!) {
        dismissKeyboard()
        if isAccessLibraryAuthorization() {
            showSelectPhotoAlertController()
        } else {
            PHPhotoLibrary.requestAuthorization({ [weak self] (newStatus) in
                if newStatus != .authorized { return }
                self?.showSelectPhotoAlertController()
            })
        }
    }
    
    @objc func pressedEditMembersButton(_ sender: UIButton!) {
        navigationController?.popViewController(animated: true)
    }
    
    func dismissKeyboard() {
        groupNameTextfield.resignFirstResponder()
    }
    
    func setupNavigationBarItem() {
        title = "建立群組"
        
        let doneBarButton = UIBarButtonItem(title: "完成", style: UIBarButtonItemStyle.plain, target: self, action: #selector(pressedDoneBarButton))
        navigationItem.setRightBarButton(doneBarButton, animated: true)
    }
    
    func setupScrollView() {
        guard let viewModelUnwrap = viewModel else { return }
        let scrollViewContentHeight = viewModelUnwrap.joinedUsersData.isEmpty ? view.frame.size.height : viewModelUnwrap.getUserViewYPosition(index: viewModelUnwrap.joinedUsersData.count - 1) + 75
        mainScrollView.contentSize = CGSize.init(width: view.frame.size.width, height: scrollViewContentHeight)
    }
    
    func dataBindingWithGroupPhoto() {
        viewModel?.groupPhotoBehaviorSubject
            .asObservable()
            .bind(to: groupPhotoImageView.rx.image)
            .disposed(by: disposeBag)
    }
    
    func setupGroupNameTextField() {
        groupNameTextfield.delegate = self
        
        guard let viewModelUnwrap = viewModel else { return }
        groupNameTextfield
            .rx
            .text
            .orEmpty
            .asObservable()
            .bind(to: viewModelUnwrap.groupNameBehaviorSubject)
            .disposed(by: disposeBag)
    }
    
    func showLoadingView() {
        guard let loadingView = UINib(nibName: "LoadingView", bundle: nil).instantiate(withOwner: nil, options: nil).first as? LoadingView
            else { return }
        groupNameTextfield.resignFirstResponder()
        self.loadingView = loadingView
        let window = UIApplication.shared.keyWindow ?? UIApplication.shared.windows.first
        window?.addSubview(loadingView)
        loadingView.snp.makeConstraints { (maker) in
            maker.width.height.equalToSuperview()
            maker.center.equalToSuperview()
        }
        loadingView.start()
    }
    
    func showSelectPhotoAlertController() {
        guard let selecPhotoAlertControllerUnwrap = selecPhotoAlertController else { return }
        present(selecPhotoAlertControllerUnwrap, animated: true, completion: nil)
    }
    
    func toImagePickerController(type: UIImagePickerControllerSourceType) {
        let imagePicker = UIImagePickerController();
        imagePicker.delegate = self;
        imagePicker.allowsEditing = true
        imagePicker.sourceType = type
        present(imagePicker, animated: true, completion: nil)
    }
    
    func layoutJoinedMembers() {
        guard let viewModelUnwrap = viewModel else { return }
        for index in 0 ..< viewModelUnwrap.joinedUsersData.count {
            layoutUserView(index: index)
        }
    }
    
    func setupSelectPhotoAlertController() {
        selecPhotoAlertController = UIAlertController(title: nil, message: "更換群組照片", preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        selecPhotoAlertController?.addAction(cancelAction)
        
        let galleryAction = UIAlertAction(title: "從相片圖庫中選取", style: .default, handler: { [weak self] (alertAction) in
            self?.toImagePickerController(type: .photoLibrary)
        })
        selecPhotoAlertController?.addAction(galleryAction)
        
        let cameraAction = UIAlertAction(title: "拍照", style: .default, handler: { [weak self] (alertAction) in
            self?.toImagePickerController(type: .camera)
        })
        selecPhotoAlertController?.addAction(cameraAction)
    }
    
    func layoutUserView(index: Int) {
        guard let userView =
            UINib(nibName: "UserTableViewCell", bundle: nil).instantiate(withOwner: nil, options: nil).first as? UserTableViewCell
            else { return }
        userView.data = viewModel?.joinedUsersData[index]
        userView.hightLightImageView.isHidden = true
        mainScrollView.addSubview(userView)
        userView.snp.makeConstraints { (maker) in
            maker.top.equalToSuperview().offset(viewModel?.getUserViewYPosition(index: index) ?? 0)
            maker.leading.equalToSuperview().offset(15)
            maker.trailing.equalTo(view.snp.trailing).offset(-15)
            maker.height.equalTo(75)
        }
    }
    
    func layoutEditableViews() {
        let editableView = UIView()
        editableView.backgroundColor = UIColor.white
        mainScrollView.addSubview(editableView)
        editableView.snp.makeConstraints { (maker) in
            maker.top.leading.width.equalToSuperview()
            maker.height.equalTo(EditGroupDetailViewController.EDIT_VIEW_HEIGHT)
        }
        
        groupPhotoImageView = UICircleImageView()
        groupPhotoImageView.backgroundColor = UIColor.HaHaGoBlue
        groupPhotoImageView.contentMode = .scaleAspectFill
        let imageViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(pressedGroupPhotoImageView(_:)))
        groupPhotoImageView.addGestureRecognizer(imageViewTapGesture)
        groupPhotoImageView.isUserInteractionEnabled = true
        editableView.addSubview(groupPhotoImageView)
        groupPhotoImageView.snp.makeConstraints { (maker) in
            maker.leading.equalToSuperview().offset(15)
            maker.centerY.equalToSuperview()
            maker.width.height.equalTo(90)
        }
        
        let cameraIconImageView = UICircleImageView(image: UIImage(named: "ic_camera"))
        cameraIconImageView.contentMode = .center
        cameraIconImageView.alpha = 0.7
        cameraIconImageView.layer.borderColor = UIColor.black.cgColor
        cameraIconImageView.layer.borderWidth = 1
        cameraIconImageView.backgroundColor = UIColor.white
        editableView.addSubview(cameraIconImageView)
        cameraIconImageView.snp.makeConstraints { (maker) in
            maker.bottom.equalTo(groupPhotoImageView)
            maker.trailing.equalTo(groupPhotoImageView)
            maker.width.height.equalTo(30)
        }
        
        groupNameTextfield = UITextField()
        groupNameTextfield.backgroundColor = UIColor.white
        groupNameTextfield.borderStyle = .none
        groupNameTextfield.placeholder = "群組名稱 (20字內)"
        groupNameTextfield.returnKeyType = .done
        editableView.addSubview(groupNameTextfield)
        groupNameTextfield.snp.makeConstraints { (maker) in
            maker.centerY.equalToSuperview().offset(-20)
            maker.leading.equalTo(cameraIconImageView.snp.trailing).offset(20)
            maker.trailing.equalToSuperview().offset(-15)
        }
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapBackgroundView(_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        let editMembersButton = UIButton()
        editMembersButton.backgroundColor = UIColor.white
        editMembersButton.layer.cornerRadius = 15
        editMembersButton.setTitle("編輯成員", for: .normal)
        editMembersButton.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        editMembersButton.setTitleColor(UIColor.HaHaGoBlue, for: .normal)
        editMembersButton.addBoarder(color: UIColor.HaHaGoBlue, width: 1)
        editMembersButton.addTarget(self, action: #selector(pressedEditMembersButton(_:)), for: .touchUpInside)
        editableView.addSubview(editMembersButton)
        editMembersButton.snp.makeConstraints { (maker) in
            maker.leading.equalTo(groupNameTextfield)
            maker.bottom.equalTo(groupPhotoImageView)
            maker.width.equalTo(groupNameTextfield).multipliedBy(0.5)
            maker.height.equalTo(cameraIconImageView)
        }
        
        let seperatorLine = UIView()
        seperatorLine.backgroundColor = UIColor.gray
        editableView.addSubview(seperatorLine)
        seperatorLine.snp.makeConstraints { (maker) in
            maker.leading.trailing.bottom.equalToSuperview()
            maker.height.equalTo(1)
        }
    }
}

extension EditGroupDetailViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.utf16.count + string.utf16.count - range.length
        return newLength <= 20
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension EditGroupDetailViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true) {
            guard let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage else { return }
            self.viewModel?.groupPhotoBehaviorSubject.onNext(selectedImage)
        }
    }
    
    func isAccessLibraryAuthorization() -> Bool {
        return PHPhotoLibrary.authorizationStatus() == .authorized
    }
}
