//
//  UserCollectionViewCell.swift
//  MemberGroups
//
//  Created by Yvonne.H on 2018/7/9.
//  Copyright © 2018年 Yvonne.H. All rights reserved.
//

import UIKit
import RxSwift

class UserCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    private(set) var disposeBag = DisposeBag()
    var data: User? {
        didSet { setCellViews() }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        photoImageView.setCircleStyle()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
    
    func setCellViews() {
        guard let dataUnwrap = data else { return }
        nameLabel.text = dataUnwrap.isPlaceHolder() ? "" : dataUnwrap.name
        photoImageView.contentMode = dataUnwrap.isPlaceHolder() ? .center : .scaleToFill
        if dataUnwrap.isPlaceHolder() {
            photoImageView.addBoarder(color: .gray)
            photoImageView.image = UIImage.init(named: "ic_plus")
        } else {
            photoImageView.removeBorder()
            photoImageView.kf.setImage(with: URL(string: dataUnwrap.photo)) {
                [weak self] (image, error, cacheType, imageURL) in
                self?.photoImageView.image = image
            }
        }
    }
}
