//
//  UserTableViewCell.swift
//  MemberGroups
//
//  Created by Yvonne.H on 2018/7/9.
//  Copyright © 2018年 Yvonne.H. All rights reserved.
//

import UIKit
import RxSwift

class UserTableViewCell: UITableViewCell {
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var hightLightImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    private(set) var disposeBag = DisposeBag()
    var data: User? {
        didSet { setCellViews() }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        photoImageView.setCircleStyle()
        hightLightImageView.setCircleStyle()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
    
    func setCellViews() {
        guard let dataUnwrap = data else { return }
        nameLabel.text = dataUnwrap.name
        idLabel.text = dataUnwrap.uid
        hightLightImageView.backgroundColor = dataUnwrap.hasJoin ? .HaHaGoBlue : .white
        photoImageView.kf.setImage(with: URL(string: dataUnwrap.photo)) {
            [weak self] (image, error, cacheType, imageURL) in
            self?.photoImageView.image = image
        }
    }
}
