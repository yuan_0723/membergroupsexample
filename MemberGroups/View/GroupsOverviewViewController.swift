//
//  GroupsOverviewViewController.swift
//  MemberGroups
//
//  Created by Yvonne.H on 2018/7/6.
//  Copyright © 2018年 Yvonne.H. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import ObjectMapper

class GroupsOverviewViewController: UIViewController {
    
    @IBOutlet weak var groupsCollectionView: UICollectionView!
    
    let disposeBag = DisposeBag()
    let viewModel = AppDelegate.getUIAppDelegate().viewModelContainer.resolve(GroupsOverviewViewModel.self)
    var groupsDataSource: RxCollectionViewSectionedReloadDataSource<SectionOfGroup>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel?.setupLoginUserObserveable()
        setupGroupsCollectionView()
        setupNavigationBarItem()
        login()
    }
    
    @IBAction func pressedCreateGroupButton(_ sender: Any) {
        guard let viewController = getVCByStoryboardID(GroupsOverviewViewModel.SELECT_GROUP_MEMBERS_VC) else {
            fatalError(getDebugInfo(MemberGroupsError.unwrapError.localizedDescription, #function, #file, #line))
        }
        pushToTargetVC(viewController)
    }
    
    func setupNavigationBarItem() {
        navigationController?.navigationBar.tintColor = UIColor.black
        title = "社群"
    }
    
    func setupGroupsCollectionView() {
        setupCellInGroupsCollectionView()
        
        viewModel?.groupsDataReplaySubject
            .asObservable()
            .bind(to: groupsCollectionView.rx.items(dataSource: groupsDataSource))
            .disposed(by: disposeBag)
        
        groupsCollectionView
            .rx
            .itemSelected
            .asDriver()
            .drive(
                onNext: { [weak self] (indexPath) in
                    guard let itemData = self?.groupsDataSource.sectionModels[indexPath.section].items[indexPath.row]
                    else { return }
                    debugPrint(itemData)
                },
                onCompleted: { debugPrint("Search Result Table View Item Select Observable Complete") },
                onDisposed: { debugPrint("Search Result Table View Item Select Observable Disposed") })
            .disposed(by: disposeBag)
        
        groupsCollectionView
            .rx
            .setDelegate(self)
            .disposed(by: disposeBag)
    }
    
    func setupCellInGroupsCollectionView() {
        groupsCollectionView.registerXIBCell(GroupsOverviewViewModel.GROUP_COLLECTION_VIEW_CELL)
        
        groupsDataSource = RxCollectionViewSectionedReloadDataSource<SectionOfGroup>(configureCell: {
            [weak self] (dataSource, collectionview, indexPath, group) in
            guard let strongSelf = self,
                let cell = strongSelf.groupsCollectionView.dequeueReusableCell(withReuseIdentifier: GroupsOverviewViewModel.GROUP_COLLECTION_VIEW_CELL, for: indexPath) as? GroupCollectionViewCell
                else {
                    debugPrint(MemberGroupsError.unwrapError.localizedDescription, #function, #file, #line)
                    return UICollectionViewCell()
            }
            cell.data = group
            return cell
        })
    }
    
    func login() {
        viewModel?.login(userFUID: GroupsOverviewViewModel.MOCK_USER_LOGIN_FUID)
            .subscribe(
                onSuccess: { [weak self] (user) in
                    guard let strongSelf = self else { return }
                    strongSelf.viewModel?.setLoginUser(user: user)},
                onError: { (error) in
                    debugPrint(getDebugInfo(error.localizedDescription, #function, #file, #line)) })
            .disposed(by: disposeBag)
    }
}

extension GroupsOverviewViewController: UICollectionViewDelegateFlowLayout {
    
    static let COLLECTION_VIEW_CWLL_OFFSET: CGFloat = 30
    static let COLLECTION_VIEW_CELL_RATIO: CGFloat = 1.4
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width
        let cellWidth = (width - GroupsOverviewViewController.COLLECTION_VIEW_CWLL_OFFSET) / 2
        return CGSize.init(width: cellWidth, height: cellWidth * GroupsOverviewViewController.COLLECTION_VIEW_CELL_RATIO)
    }
}

