//
//  CircleImageView.swift
//  MemberGroups
//
//  Created by Yvonne.H on 2018/7/11.
//  Copyright © 2018年 Yvonne.H. All rights reserved.
//

import UIKit

class UICircleImageView: UIImageView {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setCircleStyle()
    }
    
}
