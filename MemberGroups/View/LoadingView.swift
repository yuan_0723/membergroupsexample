//
//  LoadingView.swift
//  MemberGroups
//
//  Created by Yvonne.H on 2018/7/12.
//  Copyright © 2018年 Yvonne.H. All rights reserved.
//

import UIKit

class LoadingView: UIView {
    @IBOutlet weak var lodingIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var loadingLabel: UILabel!
    @IBOutlet weak var backgroundView: UIView!
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        backgroundView.layer.cornerRadius = 5
        
        let transform: CGAffineTransform = CGAffineTransform(scaleX: 4, y: 4)
        lodingIndicatorView.transform = transform
    }
    
    func start() {
        lodingIndicatorView.startAnimating()
    }
    
    func stop() {
        lodingIndicatorView.stopAnimating()
        removeFromSuperview()
    }
    
}
