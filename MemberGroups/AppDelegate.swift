//
//  AppDelegate.swift
//  MemberGroups
//
//  Created by Yvonne.H on 2018/7/6.
//  Copyright © 2018年 Yvonne.H. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let utilsContainer = DI.utilsContainer
    let viewModelContainer = DI.viewModelContainer

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }
    
    static func getUIAppDelegate() -> AppDelegate {
        return (UIApplication.shared.delegate as! AppDelegate)
    }
}

