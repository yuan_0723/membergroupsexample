//
//  User.swift
//  MemberGroups
//
//  Created by Yvonne.H on 2018/7/7.
//  Copyright © 2018年 Yvonne.H. All rights reserved.
//

import UIKit
import Foundation
import ObjectMapper
import RxDataSources

class User: NSObject {
    
    var uid = ""
    var f_uid = ""
    var name = ""
    var photo = ""
    var groupsRelationship = [String : Bool]()
    
    var hasJoin = false
    
    required init?(map: Map) {
        super.init()
    }
    
    init(uid: String = "", f_uid: String = "", name: String = "", photo: String = "") {
        super.init()
        self.uid = uid
        self.f_uid = f_uid
        self.name = name
        self.photo = photo
        self.groupsRelationship = [:]
    }
    
    func setGroupsRelationship(groupKey: String, isExist: Bool) {
        groupsRelationship.updateValue(isExist, forKey: groupKey)
    }
    
    func isPlaceHolder() -> Bool {
        return f_uid == "USERPLACEHOLDER"
    }
}

extension User: Mappable {

    func mapping(map: Map) {
        uid                <- map["uid"]
        f_uid              <- map["f_uid"]
        name               <- map["name"]
        photo              <- map["photo"]
        groupsRelationship <- map["groups"]
    }
}

extension User: IdentifiableType {
    
    typealias Identity = String
    var identity: String { return f_uid }
}

struct SectionOfUser {
    
    var header: String
    var items: [User]
}

extension SectionOfUser: SectionModelType {
    
    var identity: String {
        return header
    }
    
    init(original: SectionOfUser, items: [User]) {
        self = original
        self.items = items
    }
}

extension SectionOfUser: AnimatableSectionModelType {
    
    typealias Identity = String
}
