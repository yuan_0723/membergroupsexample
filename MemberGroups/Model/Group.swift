//
//  Group.swift
//  MemberGroups
//
//  Created by Yvonne.H on 2018/7/7.
//  Copyright © 2018年 Yvonne.H. All rights reserved.
//

import UIKit
import ObjectMapper
import RxDataSources

class Group: NSObject {
    
    var f_id = ""
    var name = ""
    var photo = ""
    var createdTS: Int64 = 0
    var users = [String: Bool]()
    
    required init?(map: Map) {
        super.init()
    }
    
    init(f_id: String = "", name: String = "", photo: String = "", users: [User]) {
        super.init()
        self.f_id = f_id
        self.name = name
        self.photo = photo
        self.createdTS = Int64(Date().timeIntervalSince1970 * 1000)
        self.users =
            users.map({ (user) -> String in return user.identity})
                 .reduce([String: Bool](), {
                    (result, userID) -> [String: Bool] in
                    var _result = result
                    _result[userID] = true
                    return _result
                 })
    }
}

extension Group: Comparable {
    static func <(lhs: Group, rhs: Group) -> Bool {
        return lhs.createdTS < rhs.createdTS
    }
}

extension Group: Mappable {
    
    func mapping(map: Map) {
        f_id      <- map["f_id"]
        name      <- map["name"]
        photo     <- map["photo"]
        users     <- map["users"]
        createdTS <- map ["created_ts"]
    }
}

struct SectionOfGroup {
    
    var header: String
    var items: [Group]
}

extension SectionOfGroup: SectionModelType {
    
    init(original: SectionOfGroup, items: [Group]) {
        self = original
        self.items = items
    }
}
