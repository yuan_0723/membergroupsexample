//
//  DI.swift
//  MemberGroups
//
//  Created by Yvonne.H on 2018/7/17.
//  Copyright © 2018年 Yvonne.H. All rights reserved.
//

import Foundation
import Swinject

class DI {
    static let utilsContainer: Container = {
        let container = Container()
        
        container.register(FirebaseUtils.self) { resolver in
            return FirebaseUtils()
        }
        
        container.register(GlobalVariableManager.self, factory: { (resolve) in
            return GlobalVariableManager()
        }).inObjectScope(.container)
        
        return container
    }()
    
    static let viewModelContainer: Container = {
        let container = Container()
        
        container.register(GroupsOverviewViewModel.self, factory: { (resolve) in
            return GroupsOverviewViewModel()
        }).inObjectScope(.container)
        
        container.register(SelectGroupMembersViewModel.self, factory: { (resolve) in
            return SelectGroupMembersViewModel()
        }).inObjectScope(.container)
        
        container.register(EditGroupDetailViewModel.self, factory: { (resolve) in
            return EditGroupDetailViewModel()
        }).inObjectScope(.container)
        
        return container
    }()
}
