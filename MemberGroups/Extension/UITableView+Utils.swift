//
//  UITableView+Utils.swift
//  MemberGroups
//
//  Created by Yvonne.H on 2018/7/10.
//  Copyright © 2018年 Yvonne.H. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    func registerXIBCell(_ nibName: String, reuseID: String? = nil) {
        self.register(UINib(nibName: nibName, bundle: nil), forCellReuseIdentifier: reuseID ?? nibName)
    }
    
    func scrollToTopRow(animated: Bool) {
        self.scrollToRow(at: IndexPath(item: 0, section: 0), at: .top, animated: animated)
    }
}

extension UICollectionView {
    func registerXIBCell(_ nibName: String, reuseID: String? = nil) {
        self.register(UINib(nibName: nibName, bundle: nil), forCellWithReuseIdentifier: reuseID ?? nibName)
    }
}
