//
//  UIView+Style.swift
//  MemberGroups
//
//  Created by Yvonne.H on 2018/7/9.
//  Copyright © 2018年 Yvonne.H. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func setCircleStyle() {
        clipsToBounds = true
        layer.masksToBounds = true
        layer.cornerRadius = self.frame.height / 2.0
    }
    
    func addShadow(color: CGColor = UIColor.black.cgColor, offset: CGSize = CGSize.zero, radius: CGFloat = 3.0, opacity: Float = 0.1, maskToBounds: Bool = false) {
        layer.shadowColor = color
        layer.shadowOffset = offset
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity
        layer.masksToBounds = maskToBounds
    }
    
    func addBoarder(color: UIColor, width: CGFloat = 2.0) {
        layer.borderColor = color.cgColor
        layer.borderWidth = width
    }
    
    func removeBorder(){
        layer.borderColor =  nil
        layer.borderWidth = 0
    }
    
}

extension UIColor {
    static let HaHaGoBlue = UIColor(red: 0/255, green: 150/255, blue: 220/255, alpha: 1)
}

extension UIViewController {
    
    func getVCByStoryboardID(_ storyboardID: String) -> UIViewController? {
        guard let viewController = self.storyboard?.instantiateViewController(withIdentifier: storyboardID) else { return nil }
        return viewController
    }
    
    func pushToTargetVC(_ viewController: UIViewController) {
        navigationController?.pushViewController(viewController, animated: true)
    }
}
