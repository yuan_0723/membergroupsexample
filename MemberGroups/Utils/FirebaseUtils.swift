//
//  FirebaseUtils.swift
//  MemberGroups
//
//  Created by Yvonne.H on 2018/7/7.
//  Copyright © 2018年 Yvonne.H. All rights reserved.
//

import UIKit
import Firebase
import ObjectMapper
import RxSwift
import FirebaseStorage

class FirebaseUtils: NSObject {
    func batchLoadData(dbReference: [DatabaseReference]) -> Observable<[Any?]> {
        let queryTasks = dbReference.map { (dbRef) -> Observable<Any?> in
            return dbRef.rx.loadData()
        }
        return Observable.zip(queryTasks)
    }
    
    func batchUpdateData(dbReference: [DatabaseReference], data: [String: Any]) -> Observable<[DatabaseReference]> {
        let updateTasks = dbReference.flatMap { (dbRef) -> Observable<DatabaseReference> in
            return dbRef.rx.updateData(data: data)
        }
        return Observable.zip(updateTasks)
    }
}

extension Reactive where Base: DatabaseReference {
    
    func updateData(data: [AnyHashable: Any]) -> Observable<DatabaseReference> {
        return Observable.create({ (observer) -> Disposable in
            self.base.updateChildValues(data, withCompletionBlock: { (error, databaseRef) in
                if let errorUnwrap = error {
                    observer.onError(errorUnwrap)
                    return
                }
                observer.onNext(databaseRef)
                observer.onCompleted()
            })
            return Disposables.create()
        })
    }
    
    func loadData() -> Observable<Any?> {
        return Observable.create({ (observer) -> Disposable in
            self.base.observeSingleEvent(of: .value, with: { (dataSnapShot) in
                observer.onNext(dataSnapShot.value)
                observer.onCompleted()
            }, withCancel: { (error) in
                observer.onError(error)
            })
            return Disposables.create()
        })
    }
}

extension Reactive where Base: StorageReference {
    func uploadData(data: Data, metadata: StorageMetadata? = nil) -> Observable<StorageMetadata> {
        return Observable.create({ (observer) -> Disposable in
            self.base.putData(data, metadata: metadata, completion: { (metadata, error) in
                if let errorUnwrap = error {
                    observer.onError(errorUnwrap)
                } else {
                    guard let metadataUnwrap = metadata else {
                        observer.onError(MemberGroupsError.unwrapError)
                        return
                    }
                    observer.onNext(metadataUnwrap)
                    observer.onCompleted()
                }
            })
            return Disposables.create()
        })
    }
    
    func getDownloadURL() -> Observable<URL> {
        return Observable.create({ (observer) -> Disposable in
            self.base.downloadURL(completion: { (url, error) in
                if let errorUnwrap = error {
                    observer.onError(errorUnwrap)
                } else {
                    guard let urlUnwrap = url else {
                        observer.onError(MemberGroupsError.unwrapError)
                        return
                    }
                    observer.onNext(urlUnwrap)
                    observer.onCompleted()
                }
            })
            return Disposables.create()
        })
    }
}

extension FirebaseUtils {
    
    enum ReferencePath: String {
        case users
        case groups
        case userDetail = "users/%@"
        case groupDetail = "groups/%@"
        case userJoinedGroup = "users/%@/groups"
    }
    
    enum CloudReferencePath: String {
        case groupPhotosStorage = "photos/groups/%@/%@"
    }
    
    func getStorageReference(path: CloudReferencePath, params: CVarArg...) -> StorageReference {
        var pathString = ""
        switch path {
        case .groupPhotosStorage:
            pathString = String(format: path.rawValue, params[0], params[1])
        }
        return pathString.isEmpty ? Storage.storage().reference() : Storage.storage().reference().child(pathString)
    }
    
    func getDBReference(path: ReferencePath, params: CVarArg...) -> DatabaseReference {
        var pathString = ""
        switch path {
        case .userDetail, .groupDetail,.userJoinedGroup:
            pathString = String(format: path.rawValue, params[0])
        default:
            pathString = path.rawValue
        }
        return pathString.isEmpty ? Database.database().reference() : Database.database().reference().child(pathString)
    }
}
