//
//  DebugHelper.swift
//  MemberGroups
//
//  Created by Yvonne.H on 2018/7/11.
//  Copyright © 2018年 Yvonne.H. All rights reserved.
//

import Foundation

public func debugPrint(_ description:String, _ function: String, _ file: String, _ line: Int) {
    debugPrint(getDebugInfo(description, function, file, line))
}

public func getDebugInfo(_ description:String, _ function: String, _ file: String, _ line: Int) -> String {
    return "Description: \(description), Function: \(function), Line:\(line), File: \(file)"
}

enum MemberGroupsError: Error {
    case unwrapError
    case injectError
}

extension MemberGroupsError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .unwrapError:
            return NSLocalizedString("Unwrap Error", comment: "MemberGroupsError")
        case .injectError:
            return  NSLocalizedString("Inject Error", comment: "MemberGroupsError")
        }
    }
}
