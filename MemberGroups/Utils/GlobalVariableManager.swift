//
//  GlobalVariableManager.swift
//  MemberGroups
//
//  Created by Yvonne.H on 2018/7/7.
//  Copyright © 2018年 Yvonne.H. All rights reserved.
//

import UIKit
import RxSwift

class GlobalVariableManager: NSObject {
    
    private var loginUser: User?
    private let loginUserPublishSubject = PublishSubject<User>()
    
    func setLoginUser(user: User) {
        loginUser = user
        loginUserPublishSubject.onNext(user)
    }
    
    func getLoginUser() -> User? {
        return loginUser
    }
    
    func getLoginUserPublishSubject() -> PublishSubject<User> {
        return loginUserPublishSubject
    }
}
