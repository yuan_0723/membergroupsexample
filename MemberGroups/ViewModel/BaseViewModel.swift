//
//  BaseViewModel.swift
//  MemberGroups
//
//  Created by Yvonne.H on 2018/7/17.
//  Copyright © 2018年 Yvonne.H. All rights reserved.
//

import Foundation

class BaseViewModel: NSObject {
    var firebaseUtils: FirebaseUtils!
    var globalVariableManager: GlobalVariableManager!
    
    override init() {
        super.init()
        do {
            firebaseUtils = try getFirebaseUtils()
            globalVariableManager = try getGlobalVariableManager()
        } catch {
            return
        }
    }
    
    private func getFirebaseUtils() throws -> FirebaseUtils  {
        guard let firebaseUtils = AppDelegate.getUIAppDelegate().utilsContainer.resolve(FirebaseUtils.self) else {
            throw MemberGroupsError.injectError
        }
        return firebaseUtils
    }
    
    private func getGlobalVariableManager() throws -> GlobalVariableManager  {
        guard let globalVariableManager = AppDelegate.getUIAppDelegate().utilsContainer.resolve(GlobalVariableManager.self) else {
            throw MemberGroupsError.injectError
        }
        return globalVariableManager
    }
}
