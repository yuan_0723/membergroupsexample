//
//  GroupsOverviewViewModel.swift
//  MemberGroups
//
//  Created by Yvonne.H on 2018/7/7.
//  Copyright © 2018年 Yvonne.H. All rights reserved.
//

import Foundation
import RxSwift
import RxDataSources
import ObjectMapper

class GroupsOverviewViewModel: BaseViewModel {
    
    static let MOCK_USER_LOGIN_FUID = "-LGxuIUMJ9DYWJUjhOOW"
    static let SELECT_GROUP_MEMBERS_VC = "SelectGroupMembersViewController"
    static let GROUP_COLLECTION_VIEW_CELL = "GroupCollectionViewCell"
    let disposeBag = DisposeBag()
    let groupsDataReplaySubject = ReplaySubject<[SectionOfGroup]>.create(bufferSize: 1)
    
    func setGroupsCollectionViewData(data: [Group]) {
        groupsDataReplaySubject.onNext([SectionOfGroup(header: "", items: data)])
    }
    
    func login(userFUID: String) -> PrimitiveSequence<SingleTrait, User> {
        return Single<User>.create(subscribe: { (singleEvent) -> Disposable in
            return self.firebaseUtils.getDBReference(path: .userDetail, params: userFUID)
                .rx
                .loadData()
                .asSingle()
                .subscribe(
                    onSuccess: { (dataSnapshot) in
                        guard let userJSON = dataSnapshot as? [String: Any],
                              let user = User(JSON: userJSON)
                        else { singleEvent(.error(MemberGroupsError.unwrapError))
                            return
                        }
                        singleEvent(.success(user))},
                    onError: { (error) in
                        singleEvent(.error(error))})
        })
    }
    
    func observeUserGroups(userFUID: String) {
        firebaseUtils?.getDBReference(path: .userJoinedGroup, params: userFUID).observe(.value, with: {
            [weak self]  (dataSnapshot) in
            guard let strongSelf = self else { return }

            guard dataSnapshot.exists(),
                  let userGroups = dataSnapshot.value as? [String: Any]
            else {
                strongSelf.setGroupsCollectionViewData(data: [])
                return
            }
            strongSelf.readGroupsDetail(groupKeys: [String] (userGroups.keys))
        }) { (error) in
            debugPrint(error)
        }
    }
    
    func readGroupsDetail(groupKeys: [String]) {
        guard let firebaseUtilsUnwrap = firebaseUtils,
              !groupKeys.isEmpty
        else { return }
        
        let groupDetailDBRef = groupKeys.map { (groupId) in
            return firebaseUtilsUnwrap.getDBReference(path: .groupDetail, params: groupId)
        }
        
        firebaseUtilsUnwrap.batchLoadData(dbReference: groupDetailDBRef)
            .asSingle()
            .map { (resultSet) -> [Group] in
                return resultSet.flatMap({ (result) -> Group? in
                    guard let groupJSON = result as? [String: Any],
                          let group = Group(JSON: groupJSON)
                    else { return nil }
                    return group})}
            .subscribe(onSuccess: { [weak self] (groups) in
                guard let strongSelf = self else { return }
                strongSelf.setGroupsCollectionViewData(data: groups.sorted())})
            { (error) in
                debugPrint(error.localizedDescription, #function, #file, #line)}
            .disposed(by: disposeBag)
    }
    
    func setupLoginUserObserveable() {
        globalVariableManager.getLoginUserPublishSubject()
            .asObservable()
            .subscribe(
                onNext: { [weak self] (user) in
                    guard let strongSelf = self else {
                        debugPrint(MemberGroupsError.unwrapError.localizedDescription, #function, #file, #line)
                        return
                    }
                    strongSelf.observeUserGroups(userFUID: user.f_uid) },
                onError: { (error) in
                    debugPrint(MemberGroupsError.unwrapError.localizedDescription, #function, #file, #line)})
            .disposed(by: disposeBag)
    }
    
    func setLoginUser(user: User) {
        if let currentUser = globalVariableManager.getLoginUser() {
            firebaseUtils.getDBReference(path: .userDetail, params: currentUser.f_uid).removeAllObservers()
        }
        globalVariableManager.setLoginUser(user: user)
    }
}
