//
//  EditGroupDetailViewModel.swift
//  MemberGroups
//
//  Created by Yvonne.H on 2018/7/11.
//  Copyright © 2018年 Yvonne.H. All rights reserved.
//

import UIKit
import RxSwift
import FirebaseDatabase

class EditGroupDetailViewModel: BaseViewModel {
    var joinedUsersData = [User]()
    let groupNameBehaviorSubject = BehaviorSubject(value: "")
    let groupPhotoBehaviorSubject = BehaviorSubject<UIImage?>(value: nil)
    let disposebag = DisposeBag()
    
    func setupJoinedUsersData(otherUsers: [User]) {
        let loginUser = globalVariableManager.getLoginUser()
        joinedUsersData = loginUser == nil ? otherUsers :  [loginUser!] + otherUsers
    }
    
    func getUserViewYPosition(index: Int) -> CGFloat {
        return CGFloat(index == 0 ? 130 : 130 + (75 * index))
    }
    
    func generateNewGroupData() -> Group {
        let groupNameUnwrap = (try? groupNameBehaviorSubject.value()) ?? ""
        let groupAutoID = firebaseUtils.getDBReference(path: .groups, params: []).childByAutoId().key
        return Group(f_id: groupAutoID, name: groupNameUnwrap.isEmpty ? "未命名" : groupNameUnwrap, users: joinedUsersData)
    }
    
    func batchUpdateUserJoinedData(groupID: String) -> PrimitiveSequence<SingleTrait, [DatabaseReference]> {
        let userJoinedGroupsDBRefPath = joinedUsersData.flatMap { (user) in
            return firebaseUtils.getDBReference(path: .userJoinedGroup, params: user.identity)
        }
        return firebaseUtils.batchUpdateData(dbReference: userJoinedGroupsDBRefPath, data: [groupID: true]).asSingle()
    }
    
    func uploadGroupPhoto(groupID: String, photoData: Data) -> Observable<URL> {
        let photoStorageREF = firebaseUtils.getStorageReference(path: .groupPhotosStorage, params: groupID, NSUUID().uuidString)
        return
            photoStorageREF
                .rx
                .uploadData(data: photoData)
                .flatMap { _ in return photoStorageREF.rx.getDownloadURL()
        }
    }
    
    func createGroupData(group: Group) -> PrimitiveSequence<SingleTrait, [DatabaseReference]> {
        return
            firebaseUtils.getDBReference(path: .groups, params: [])
                .rx
                .updateData(data: [group.f_id: group.toJSON()])
                .asSingle()
                .flatMap {
                    [weak self] (groupDBRef) -> PrimitiveSequence<SingleTrait, [DatabaseReference]> in
                    guard let strongSelf = self else { throw MemberGroupsError.unwrapError }
                    return strongSelf.batchUpdateUserJoinedData(groupID: group.f_id)
        }
    }
    
    func isNeedUploadPhotoData() -> (isNeed: Bool, photoData: Data) {
        guard let groupPhoto = try? groupPhotoBehaviorSubject.value(),
            let groupPhotoUnwrap = groupPhoto as UIImage!,
            let photoData = UIImageJPEGRepresentation(groupPhotoUnwrap, 0.2)
            else {
                return (false, Data())
        }
        return (true, photoData)
    }
    
    func getCreateGroupDataObservable() -> Single<Group> {
        return Single<Group>.create(subscribe: { [weak self] (singleEvent) -> Disposable in
            
            guard let strongSelf = self else {
                singleEvent(.error(MemberGroupsError.unwrapError))
                return Disposables.create()
            }
            
            let newGroupData = strongSelf.generateNewGroupData()
            let uploadPhotoCheck = strongSelf.isNeedUploadPhotoData()
            var createEventDisposable = strongSelf.createGroupData(group: newGroupData)
            
            if uploadPhotoCheck.isNeed {
                createEventDisposable =
                    strongSelf.uploadGroupPhoto(groupID: newGroupData.f_id, photoData: uploadPhotoCheck.photoData)
                        .flatMap({
                            (url) -> PrimitiveSequence<SingleTrait, [DatabaseReference]> in
                            newGroupData.photo = url.absoluteString
                            return strongSelf.createGroupData(group: newGroupData)})
                        .asSingle()
            }
            
            createEventDisposable
                .subscribe(
                    onSuccess: { (dbRef) in
                        singleEvent(.success(newGroupData))},
                    onError: { (error) in
                        singleEvent(.error(error))})
                .disposed(by: strongSelf.disposebag)
            return Disposables.create()
        })
    }
}
