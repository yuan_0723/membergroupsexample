//
//  SelectGroupMembersViewModel.swift
//  MemberGroups
//
//  Created by Yvonne.H on 2018/7/9.
//  Copyright © 2018年 Yvonne.H. All rights reserved.
//

import UIKit
import RxSwift
import RxDataSources

class SelectGroupMembersViewModel: BaseViewModel {
    
    static let SELECTED_USER_PLACE_HOLDER = User(uid: "", f_uid: "USERPLACEHOLDER", name: "", photo: "")
    var searchResultReplaySubject = ReplaySubject<[SectionOfUser]>.create(bufferSize: 1)
    var selectedUsersBehaviorSubject = BehaviorSubject<[SectionOfUser]>(value: [SectionOfUser(header: "", items: [SelectGroupMembersViewModel.SELECTED_USER_PLACE_HOLDER])])
    var otherUsers = [User]()
    var searchResultData = [User]()
    var selectedUsersData = [User]()
    let disposeBag = DisposeBag()

    override init() {
        super.init()
        loadOtherUsersData()
    }
    
    func initSelectedUsersData() {
        setSelectedUsersCollectionViewData(data: [SelectGroupMembersViewModel.SELECTED_USER_PLACE_HOLDER])
    }
    
    func searchUsersData(searchText: String) {
        let filterData = searchText.isEmpty
            ? otherUsers
            : otherUsers.filter({ (user) -> Bool in
                return user.name.contains(searchText) || user.uid.contains(searchText)
        })
        setUsersFilterTableViewData(data: filterData)
    }
    
    func setUsersFilterTableViewData(data: [User]) {
        searchResultData = data
        searchResultReplaySubject.onNext([SectionOfUser(header: "", items: data)])
    }
    
    func setSelectedUsersCollectionViewData(data: [User]) {
        selectedUsersData = data
        selectedUsersBehaviorSubject.onNext([SectionOfUser(header: "", items: data)])
    }
    
    func loadOtherUsersData() {
        firebaseUtils?.getDBReference(path: .users, params: [])
            .rx
            .loadData()
            .asSingle()
            .subscribe(
                onSuccess: { [weak self] (dataSnapshot) in
                    guard let strongSelf = self else { return }
                    guard dataSnapshot != nil,
                          let usersData = dataSnapshot as? [String: Any]
                    else {
                        strongSelf.otherUsers = []
                        strongSelf.setUsersFilterTableViewData(data: [])
                        return
                    }
                    
                    strongSelf.otherUsers = usersData.flatMap({ (element) -> User? in
                        guard let value = element.value as? [String: Any] else { return nil }
                        let user = User(JSON: value)
                        if let loginUser = strongSelf.globalVariableManager.getLoginUser() {
                            return loginUser.f_uid != user?.f_uid ? user : nil
                        }
                        return user
                    })
                    
                    strongSelf.setUsersFilterTableViewData(data: strongSelf.otherUsers)},
                onError: { (error) in
                    debugPrint("Load Other User Data Error", error.localizedDescription)})
            .disposed(by: disposeBag)
    }
    
    func updateUserJoinStatus(user: User, willUserJoin: Bool) {
        
        let updateUsersData = otherUsers.map { (oldUser) -> User in
            if user.f_uid == oldUser.f_uid { oldUser.hasJoin = willUserJoin }
            return oldUser
        }
        otherUsers = updateUsersData

        let newFilterUserData = searchResultData.map { (oldUser) -> User in
            if user.f_uid == oldUser.f_uid { oldUser.hasJoin = willUserJoin }
            return oldUser
        }
        setUsersFilterTableViewData(data: newFilterUserData)
        
        
        if willUserJoin {
            if selectedUsersData.isEmpty || selectedUsersData[0].isPlaceHolder() {
                selectedUsersData.removeAll()
            }
            setSelectedUsersCollectionViewData(data: selectedUsersData + [user])
        } else {
            var newSelectedUsersData = selectedUsersData.filter({ (oldUser) -> Bool in
                return oldUser != user
            })
            if newSelectedUsersData.isEmpty {
                newSelectedUsersData.append(SelectGroupMembersViewModel.SELECTED_USER_PLACE_HOLDER)
            }
            setSelectedUsersCollectionViewData(data: newSelectedUsersData)
        }
    }
}
